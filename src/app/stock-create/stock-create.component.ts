import { Component, OnInit,Input } from '@angular/core';
import { Router } from '@angular/router';
import { RestApiService } from "../shared/rest-api.service";

@Component({
  selector: 'app-stock-create',
  templateUrl: './stock-create.component.html',
  styleUrls: ['./stock-create.component.css']
})
export class StockCreateComponent implements OnInit {

    @Input() stockDetails = {id : 0,stockTicker : "",stockName : "", price : 0,volume : 0 ,buyOrSell : "",
    statusCodeCity : 0,date_Time : "" }

  constructor(
    public restApi: RestApiService, 
    public router: Router
  ) { }

  ngOnInit() { }

  addStock() {
    this.restApi.createStock(this.stockDetails).subscribe((data: {}) => {
      this.router.navigate(['/stock-list'])
    })
  }

}